package lab1;

public class circle {

	public double x;
	public double y;
	public double r;
	public static void main(String args[]) {	
		
		circle circ= new circle();
		printSummary(0, circ);
		circ.setX(-2.06);
		circ.setY(-0.23);
		circ.setR(1.33);
		printSummary(1, circ);
		circ.setX(0.0);
		circ.setY(5.0);
		circ.setR(1.88);
		printSummary(2, circ);
		circ.translate(5.0, 0.0);
		printSummary(3, circ);
		System.out.println("Area: "+ circ.getArea());
		circ.scale(0.5);
		printSummary(4, circ);
		circ.translate(-1.0, -1.0);
		circ.setR(3.1);
		printSummary(5, circ);
	}
	
	public static void printSummary(int state, circle circ) {
		System.out.println("------------------------------");
		System.out.println("State: "+ state + " , X: "+ circ.getX());
		System.out.println("State: "+ state + " , Y: "+ circ.getY());
		System.out.println("State: "+ state + " , R: "+ circ.getR());
	}
		public double getX() {
			return this.x;
	}	
		public void setX(double x) {
			this.x= x;
		}
		public double getY() {
			return this.y;
		}
		public void setY(double y) {
			this.y= y;
		}
		public double getR() {
			return this.r;
		}
		public void setR(double r) {
			this.r= r;
		}
		public double getArea() {
			return Math.PI*Math.pow(this.r, 2);
			//  ?   return Math.PI*r*r;
		}
		public void translate(double dx, double dy) {
			this.x += dx;
			this.y += dy;
		}
		public double scale(double scaler) {
			this.r *= scaler;
			return this.r;
		}
	/*public void setX(double newX) {
		x= newX;
	} */
	
}
