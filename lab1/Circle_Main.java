package lab1;
import lab1.*; 
public class Circle_Main {

	public static void main(String[] args) {
		circle circ= new circle();
		circle.printSummary(0, circ);
		circ.setX(-2.06);
		circ.setY(-0.23);
		circ.setR(1.33);
		circle.printSummary(1, circ);
		circ.setX(0.0);
		circ.setY(5.0);
		circ.setR(1.88);
		circle.printSummary(2, circ);
		circ.translate(5.0, 0.0);
		circle.printSummary(3, circ);
		System.out.println("Area: "+ circ.getArea());
		circ.scale(0.5);
		circle.printSummary(4, circ);
		circ.translate(-1.0, -1.0);
		circ.setR(3.1);
		circle.printSummary(5, circ);

	}

}
